import { create } from 'zustand';
import { CONDITION_BLOCKS } from '../constants/constants';
import { BlocksStoreType } from '../types/BlocksStore.Type';

export const useBlocksStore = create<BlocksStoreType>(set => ({
	blocks: CONDITION_BLOCKS,
	addBlock: block => set(state => ({ blocks: [block,...state.blocks] })),
	removeBlock: (id) => set(state => ({ blocks: state.blocks.filter(e => e.id !== id) })),
  editBlock: (id,block) => set(state => ({
    blocks: state.blocks.map((e) => {
      return e.id == id ? { ...block } : e
  }) }))
}));