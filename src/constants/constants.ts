export const CONDITION_BLOCKS = [
	{ id:1, width: 5, height: 7, count: 50 },
	{ id:2, width: 3, height: 4.5, count: 70 },
	{ id:3, width: 9, height: 2, count: 50 }
];

export const COLORS = [
	'rgb(241, 3, 3)',
	'rgb(2, 133, 2)',
	'rgb(1, 1, 247)',
	'rgb(136, 4, 4)',
	'rgb(100, 168, 10)',
	'rgb(17, 17, 83)'
];