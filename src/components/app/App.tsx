import { Route, createBrowserRouter, createRoutesFromElements, RouterProvider } from 'react-router-dom';
import Layout from '../../pages/layout/Layout';
import HomePage from '../../pages/home-page/home-page';
import ErrorPage from '../../pages/error-page/error-page';
import BasicAlgorithmPage from '../../pages/basic-algorithm-page/basic-algorithm-page';
import AdvancedAlgorithmPage from '../../pages/advanced-algorithm-page/advanced-algorithm-page';

const router = createBrowserRouter(
	createRoutesFromElements(
		<Route path="/" element={<Layout />}>
			<Route index element={<HomePage />} />
			<Route path="basic-algorithm" element={<BasicAlgorithmPage />} />
			<Route path="advanced-algorithm" element={<AdvancedAlgorithmPage />} />
			<Route path="*" element={<ErrorPage />} />
		</Route>
	)
);

export default function App(): JSX.Element {
	return <RouterProvider router={router} />
}
