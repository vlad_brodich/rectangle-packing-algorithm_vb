import style from './Footer.module.css'

export default function Footer(): JSX.Element {
	return (
		<footer className={style.footer}>
			<div className={style.footer_wrapper}>
				<a className={style.link} href="mailto:brodich_vlad@ukr.net">brodich_vlad@ukr.net</a>
				<p>3 Лютого 2024р.</p>
			</div>
		</footer>
	);
}