import style from './modal-form.module.css';
import { useEffect, useState } from 'react';
import { useForm, SubmitHandler } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';

import { COLORS } from '../../constants/constants';
import { useBlocksStore } from '../../store/store';

import { createId } from '../../functions/create_id';
import { wordPluralEndingsUk } from '../../functions/wordPluralEndingsUk';

import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Form from 'react-bootstrap/Form';

import { FormSchema, formSchema } from '../../types/FormSchema ';
import { InputBlock } from '../../types/InputBlock.Type';
import { ModalFormProps } from '../../types/ModalFormProps.Type';

export default function ModalForm(props: ModalFormProps) {
	const { id } = props;

	// Стан.
	const blocks = useBlocksStore(state => state.blocks);
	const addBlock = useBlocksStore(state => state.addBlock);
	const editBlock = useBlocksStore(state => state.editBlock);

	// Валідація.
	const {
		register,
		handleSubmit,
		formState: { errors }
	} = useForm<FormSchema>({ resolver: zodResolver(formSchema) });

	// Стан інпутів.
	const [blockWidth, setBlockWidth] = useState<number | null>(null);
	const [blockHeight, setBlockHeight] = useState<number | null>(null);
	const [blockСount, setBlockСount] = useState<number | null>(null);

	// Функція створення нової фігури.
	const createNewBlock = (width: number, height: number, count: number): InputBlock => {
		return {
			id: createId(),
			width,
			height,
			count
		};
	};
	// Оновлення стану інпутів в залежності від id.
	useEffect(() => {
		if (id !== null) {
			const block = blocks.find(e => e.id == id);
			if (block) {
				setBlockWidth(block.width);
				setBlockHeight(block.height);
				setBlockСount(block.count);
			}
		} else {
			setBlockWidth(2);
			setBlockHeight(2);
			setBlockСount(1);
		}
	}, []);

	// Обробник відправки форми.
	const onSubmit: SubmitHandler<FormSchema> = data => {
		if (id) {
			editBlock(id, { id, ...data });
		} else {
			addBlock(createNewBlock(data.width, data.height, data.count));
		}
		props.onHide();
	};
	// Функції зміни стану інпутів.
	const setWidth = (value: number): void => {
		value >= 2 && value <= 20 && setBlockWidth(value);
	};
	const setHeight = (value: number): void => {
		value >= 2 && value <= 40 && setBlockHeight(value);
	};
	const setСount = (value: number): void => {
		value >= 1 && setBlockСount(value);
	};
	
	return (
		<Modal {...props} size="lg" className={style.modal} aria-labelledby="contained-modal-title-vcenter" centered>
			<Modal.Header closeButton>
				<Modal.Title className={style.title} id="contained-modal-title-vcenter">
					{id == null ? 'Створити блок' : 'Змінити блок'}
				</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<Form className={style.form} onSubmit={handleSubmit(onSubmit)}>
					{blockWidth && (
						<Form.Group className={['mb-3', style.form_group].join(' ')} controlId="form.ControlWidth">
							<Form.Label>Ширина.</Form.Label>
							<Form.Control
								className={[style.input, errors.width && style.input_error].join(' ')}
								{...register('width', { setValueAs: w => parseFloat(w) })}
								type="number"
								defaultValue={blockWidth}
								step={0.1}
								onChange={e => {
									setWidth(parseFloat(e.target.value));
								}}
							/>

							<Form.Label className={errors.width ? style.label_error : style.label}>
								{errors.width ? errors.width?.message : wordPluralEndingsUk(blockWidth, 'Дюйм')}
							</Form.Label>
						</Form.Group>
					)}
					{blockHeight && (
						<Form.Group className={['mb-3', style.form_group].join(' ')} controlId="form.ControlHeight">
							<Form.Label>Висота.</Form.Label>
							<Form.Control
								{...register('height', { setValueAs: w => parseFloat(w) })}
								type="number"
								defaultValue={blockHeight}
								className={[style.input, errors.height && style.input_error].join(' ')}
								step={0.1}
								onChange={e => {
									setHeight(parseFloat(e.target.value));
								}}
							/>
							<Form.Label className={errors.height ? style.label_error : style.label}>
								{errors.height ? errors.height?.message : wordPluralEndingsUk(blockHeight, 'Дюйм')}
							</Form.Label>
						</Form.Group>
					)}
					{blockСount && (
						<Form.Group className={['mb-3', style.form_group].join(' ')} controlId="form.ControlСaunt">
							<Form.Label>Кількість.</Form.Label>
							<Form.Control
								{...register('count', {
									setValueAs: v => Number(v)
								})}
								className={[style.input, errors.count && style.input_error].join(' ')}
								type="number"
								defaultValue={blockСount}
								onChange={e => {
									setСount(Number(e.target.value));
								}}
							/>
							<Form.Label className={errors.count ? style.label_error : style.label}>
								{errors.count ? errors.count?.message : 'Шт.'}
							</Form.Label>
						</Form.Group>
					)}
					<Button size="lg" variant="success" type="submit">
						Зберегти
					</Button>
				</Form>
			</Modal.Body>
			<Modal.Footer className={style.foter}>
				<div
					style={{
						width: `${blockWidth && blockWidth * 10}px`,
						height: `${blockHeight && blockHeight * 10}px`,
						backgroundColor: (COLORS[0] ??= 'rgb(100, 168, 10)'),
						animation: ' block 1s linear'
					}}
					className={style.block}
				>
					{blockСount && blockСount}
				</div>
			</Modal.Footer>
		</Modal>
	);
}
