import style from './Block.module.css'
import { sizeAdapter } from '../../functions/sizeAdapter';
import { BlockPropTypes } from '../../types/BlockProps.Type';

export default function Block({ width, height, numBlock, x, y, color, indexSize }: BlockPropTypes): JSX.Element {
	return (
		<div
			style={{
				width: `${sizeAdapter(width, indexSize)}px`,
				height: `${sizeAdapter(height, indexSize)}px`,
				position: 'absolute',
				top: `${sizeAdapter(y, indexSize)}px`,
				left: `${sizeAdapter(x, indexSize)}px`,
				backgroundColor: color,
				animation: ' block 3s linear',
				fontSize: `${sizeAdapter(10, indexSize)}px`
			}}
			className={style.block}
		>
			{numBlock}
		</div>
	);
}
