import style from './ListResult.module.css'
import { sizeAdapter } from '../../functions/sizeAdapter';
import Block from '../block/Block';

import { ListResultProps } from '../../types/ListResultProps.Type';

export default function ListResult({ sheets, indexSize }: ListResultProps): JSX.Element {

	return (
		<ul className={style.sheets}>
			{sheets.map((sheet, index) => (
				<li key={index} className={style.sheet}>
					<h2>{`Аркуш: ${sheet.numSheet}`}</h2>
					<p>{`Розмір аркушу ${sheet.boxSize.w}x${sheet.boxSize.h}`}</p>
					{indexSize > 0 ? <p>{`Маштаб відображення: ${(indexSize*100).toFixed(2)}%`}</p> : null}
					<div
						className={style.sheet_result}
						style={{
							width: `${sizeAdapter(sheet.boxSize.w, indexSize) + 4}px`,
							height: `${sizeAdapter(sheet.boxSize.h, indexSize) + 4}px`
						}}
					>
						{sheet.blocks.map((block, i) => (
							<Block
								key={i}
								width={block.w}
								height={block.h}
								numBlock={block.num}
								x={block.x}
								y={block.y}
								color={block.color}
								indexSize={indexSize}
							/>
						))}
					</div>
					<p>{`Вільний простір: ${sheet.empty.toFixed(2)}%`}</p>
				</li>
			))}
		</ul>
	);
}
