import style from './figures-list.module.css';

import { COLORS } from '../../constants/constants';
import { useBlocksStore } from '../../store/store';

import { Button } from 'react-bootstrap';

import { FiguresListProps } from '../../types/FiguresListProps.Type';

export default function FiguresList({ data, callbackModalShow }: FiguresListProps): JSX.Element {
	const removeBlock = useBlocksStore(state => state.removeBlock);

	const blocks = data.map((el, i) => {
		return (
			<li className={style.list_item} key={el.id}>
				<p>{`${i + 1}. ${el.width} inch x ${el.height} inch | ${el.count} шт.`}</p>
				<div
					style={{
						width: `${el.width * 10}px`,
						height: `${el.height * 10}px`,
						backgroundColor: (COLORS[i] ??= 'rgb(100, 168, 10)'),
						animation: ' block 1s linear'
					}}
					className={style.block}
				>
					{el.count}
				</div>
				<div>
					<Button
						variant="warning"
						className={style.btn}
						onClick={() => {
							callbackModalShow(el.id);
						}}
						type="button"
					>
						Редагувати
					</Button>
					<Button
						variant="danger"
						className={style.btn}
						onClick={() => {
							removeBlock(el.id);
						}}
						type="button"
					>
						Видалити
					</Button>
				</div>
			</li>
		);
	});

	return <ul className={style.list}>{blocks}</ul>;
}
