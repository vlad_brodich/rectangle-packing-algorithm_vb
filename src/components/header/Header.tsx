import style from './Header.module.css';
import { useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { HiMenu } from 'react-icons/hi';

export default function Header(): JSX.Element {
	const pathname = useLocation().pathname;
	const [open, setOpen] = useState<boolean>(false);

	const body: HTMLBodyElement | null = document.querySelector('body');
	// Блокуемо скрол якщо меню відчинене.
	useEffect(() => {
		if (open) {
			body?.classList.add('hiden');
		} else {
			body?.classList.remove('hiden');
		}
	}, [open]);

	return (
		<header className={style.header}>
			<nav className={style.header_wrap}>
				<Link className={style.logo} to={'/'}>
					<span className={style.logo_yllow}>V</span>
					<span className={style.logo_blue}>B</span>
				</Link>
				<ul
					className={[style.nav_list, open ? style._activ : ''].join(' ')}
					onClick={() => {
						setOpen(false);
					}}
				>
					<li>
						<Link className={[style.link, pathname == '/' ? style.active : ''].join(' ')} to={'/'}>
							На головну
						</Link>
					</li>
					<li>
						<Link
							className={[style.link, pathname == '/basic-algorithm' ? style.active : ''].join(' ')}
							to={'basic-algorithm'}
						>
							Базовий алгоритм
						</Link>
					</li>
					<li>
						<Link
							className={[style.link, pathname == '/advanced-algorithm' ? style.active : ''].join(' ')}
							to={'advanced-algorithm'}
						>
							Складний алгоритм
						</Link>
					</li>
				</ul>
				<button
					onClick={() => {
						setOpen(true);
					}}
					className={style.btn_menu}
					type="button"
				>
					<HiMenu className={style.btn_menu_icon} />
				</button>
			</nav>
		</header>
	);
}
