import style from './select.module.css'
import Form from 'react-bootstrap/Form';

import { SelectProps } from '../../types/SelectProps.Type';

export default function Select({ selected }: SelectProps): JSX.Element {
	return (
		<Form.Select
			onChange={e => {
				selected(e.target.value);
			}}
			aria-label="Default select example"
			className={style.select}
		>
			<option>Оберіть спосіб сортування фігур.</option>
			<option value="maxH">Максимальна висота фігури.</option>
			<option value="minH">Мінімальна висота фігури.</option>
			<option value="maxW">Максимальна ширина фігури.</option>
			<option value="minW">Мінімальна ширина фігури.</option>
			<option value="maxS">Максимальна площа фігури.</option>
			<option value="minS">Мінімальна площа фігури.</option>
			<option value="random">Випадкове тасування фігур.</option>
		</Form.Select>
	);
}


