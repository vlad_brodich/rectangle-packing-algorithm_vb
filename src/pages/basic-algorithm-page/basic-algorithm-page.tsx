import style from './basic-algorithm-page.module.css';

import { useEffect, useState } from 'react';
import { useBlocksStore } from '../../store/store';

import createDataBlocks from '../../functions/create-data-blocks';
import BasicPacker from '../../functions/basic-packer';
import { sortBlocks } from '../../functions/sortBlocks';

import Select from '../../components/select/select';
import ListResult from '../../components/list-result/ListResult';

import { SheetType } from '../../types/SheetType.Type';
import { BlockType } from '../../types/BlockType.Type';

export default function BasicAlgorithmPage(): JSX.Element {
	// Стан.
	const blocksStore = useBlocksStore(state => state.blocks);
	// Локальний стан.
	const [blocks, setBlocks] = useState<BlockType[]>([]);
	const [pages, setPages] = useState<SheetType[]>([]);
	const [numPage, setNumPage] = useState(1);

	// Прокручування сторінки на початок.
	window.scrollTo({
		top: 0,
		behavior: 'smooth'
	});
	
	// Початкове сортування вхідних блоків.
	useEffect(() => {
		setBlocks(sortBlocks(createDataBlocks(blocksStore), 'maxH'));
	}, []);
	// Створення нових аркушів поки не будуть задіяні всі блоки.
	useEffect(() => {
		if (blocks.length) {
			createSheets(blocks, 200, 400);
		}
	}, [blocks]);

	// Функія створення нового аркуша.
	function createSheets(data: BlockType[], w: number, h: number) {
		const packer = new BasicPacker(w, h);
		packer.fit(data);
		const boxSize = packer.getBoxSize();

		const areaPage = w * h;
		const onePercent = areaPage / 100;
		let empty = 0;
		const nofitBlocks = [];
		const blocks = [];

		for (let n = 0; n < data.length; n++) {
			let block = data[n];
			if (block.fit) {
				blocks.push({ x: block.fit.x, y: block.fit.y, w: block.w, h: block.h, num: block.num, color: block.color });
				empty += block.w * block.h;
			} else {
				nofitBlocks.push(block);
			}
		}
		setPages([...pages, { blocks, boxSize, numSheet: numPage, empty: (areaPage - empty) / onePercent }]);
		setNumPage(numPage + 1);
		setBlocks([...nofitBlocks]);
	}

	// Функія вибору способу сортування.
	function selected(value: string): void {
		setNumPage(1);
		setPages([]);
		setBlocks(sortBlocks(createDataBlocks(blocksStore), value));
	}

	return (
		<main className={style.main}>
			<h1 className={style.title}>Базовий алгоритм упаковки прямокутників.</h1>
			<div className={style.select_wrap}>
				<p>
					Найкращі результати досягаються, коли вхідні блоки сортуються за максимальною висотою. <br />
					Спробуйте самі
				</p>
				<Select selected={selected} />
			</div>

			{pages.length ? <ListResult sheets={pages} indexSize={0} /> : null}
		</main>
	);
}
