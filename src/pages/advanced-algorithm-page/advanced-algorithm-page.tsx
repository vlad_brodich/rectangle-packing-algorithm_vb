import style from './advanced-algorithm-page.module.css';

import { useEffect, useState } from 'react';
import { useBlocksStore } from '../../store/store';

import { AdvancedPacker } from '../../functions/advanced-packer';
import createDataBlocks from '../../functions/create-data-blocks';
import { sortBlocks } from '../../functions/sortBlocks';

import ListResult from '../../components/list-result/ListResult';
import Select from '../../components/select/select';

import { BlockType } from '../../types/BlockType.Type';
import { SheetType } from '../../types/SheetType.Type';

export default function AdvancedAlgorithmPage(): JSX.Element {
	// Стан.
	const blocksStore = useBlocksStore(state => state.blocks);
	// Локальний стан.
	const [blocks, setBlocks] = useState<BlockType[]>([]);
	const [pages, setPages] = useState<SheetType[]>([]);
	const [numPage, setNumPage] = useState(1);
	// Медіа запит Ширина екрану користувача.
	const clientWidth = document.documentElement.clientWidth;
	const [indexSize, setIndexSize] = useState<number>(0);

	// Прокручування сторінки на початок.
	window.scrollTo({
		top: 0,
		behavior: 'smooth'
	});
	
	// Початкове сортування вхідних блоків.
	useEffect(() => {
		setBlocks(sortBlocks(createDataBlocks(blocksStore), 'maxS'));
	}, []);

	// Створення нових аркушів поки не будуть задіяні всі блоки.
	useEffect(() => {
		if (blocks.length) {
			createSheets(blocks);
		}
	}, [blocks]);

	// Функія створення нового аркуша.
	function createSheets(data: BlockType[]) {
		const packer = new AdvancedPacker();
		packer.fit(data);
		const boxSize = packer.getBoxSize();
		// Визначаємо маштаб відоьраження.
		if (clientWidth - 50 < boxSize.w) {
			setIndexSize((clientWidth - 50) / boxSize.w);
		}

		const areaPage = boxSize.w * boxSize.h;
		const onePercent = areaPage / 100;
		let empty = 0;
		const nofitBlocks = [];
		const blocks = [];

		for (let n = 0; n < data.length; n++) {
			let block = data[n];
			if (block.fit) {
				blocks.push({ x: block.fit.x, y: block.fit.y, w: block.w, h: block.h, num: block.num, color: block.color });
				empty += block.w * block.h;
			} else {
				nofitBlocks.push(block);
			}
		}
		setPages([...pages, { blocks, boxSize, numSheet: numPage, empty: (areaPage - empty) / onePercent }]);
		setNumPage(numPage + 1);
		setBlocks([...nofitBlocks]);
	}

	// Функія вибору способу сортування.
	function selected(value: string): void {
		setNumPage(1);
		setPages([]);
		setBlocks(sortBlocks(createDataBlocks(blocksStore), value));
	}

	return (
		<main className={style.main}>
			<h1 className={style.title}>Складний алгоритм упаковки прямокутників.</h1>
			<div className={style.select_wrap}>
				<p>
					Найкращі результати досягаються, коли вхідні блоки сортуються за максимальною площею.
					<br />
					Спробуйте самі
				</p>
				<Select selected={selected} />
			</div>
			{pages.length ? <ListResult indexSize={indexSize} sheets={pages} /> : null}
			{blocks.length > 0 ? <p>{`Не влізло блоків: ${blocks.length} шт.`}</p> : null}
		</main>
	);
}
