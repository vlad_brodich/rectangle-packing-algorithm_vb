import style from './error-page.module.css'
import { Link, useRouteError } from 'react-router-dom';

export default function ErrorPage(): JSX.Element {
	  const error = useRouteError();
		console.error(error);
	return (
		<main className={style.main}>
			<div className={style.error_content}>
				<h1>Oops!</h1>
				<p>На жаль, такої сторінки не існує.</p>
				{error ? <p>{`Error: ${error}`}.</p> : null}
				<Link className={style.link} to={'/'}>
					На головну сторінку.
				</Link>
			</div>
		</main>
	);
}