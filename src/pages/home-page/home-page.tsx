import style from './home-page.module.css';
import { useState } from 'react';

import { useBlocksStore } from '../../store/store';

import { wordPluralEndingsUk } from '../../functions/wordPluralEndingsUk';

import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import FiguresList from '../../components/figures-list/figures-list';
import ModalForm from '../../components/modal-form/modal-form';

export default function HomePage(): JSX.Element {
	// Стан.
	const blocks = useBlocksStore(state => state.blocks);
	// Локальний стан.
	const [modalShow, setModalShow] = useState<boolean>(false);
	const [BlockId, setBlockId] = useState<string | number | null>(null);

	// Прокручування сторінки на початок.
	window.scrollTo({
		top: 0,
		behavior: 'smooth'
	});

	// Фукція модального вікна.
	const callbackModalShow = (id: string | number): void => {
		setBlockId(id);
		setModalShow(true);
	};

	return (
		<main className={style.main}>
			<h1 className={style.title}>Тестове завдання на посаду Junior Frontend Developer</h1>
			<section className={style.task_section}>
				<h2 className={style.section_title}>Умови:</h2>
				<ul className={style.task_list}>
					<li className={style.task_list_item}>В нас є аркуш 20 x 40 дюймів (inch)</li>
					<li className={style.task_list_item}>
						<div className={style.wrapper_btns}>
							<h3>{`Та ${blocks.length} ${wordPluralEndingsUk(
								blocks.length,
								'блок'
							)} з відповідними розмірами і кількостями:`}</h3>
							<Button
								size="lg"
								variant="success"
								onClick={() => setModalShow(true)}
								type="button"
								className={style.btn}
							>
								+ <span className={style.btn_text}>Додати блок</span>
							</Button>
						</div>

						<FiguresList callbackModalShow={callbackModalShow} data={blocks} />
					</li>
					<li className={style.task_list_item}>
						1. Треба зробити алгоритм який розташує ці блоки в аркуші максимально ергономічно (щоб як умога більше місця
						було задіяно) та видасть найменьшу можливу кількість аркушів.
					</li>
					<li className={style.task_list_item}>
						2. Треба зробити алгоритм який розташує ці блоки в одному аркуші максимально ергономічно (щоб як умога
						більше місця було задіяно)та видасть найменьш можливий розмір аркуша максимально квадратної форми.
					</li>
					<li className={style.task_list_item}>3. Треба вивести результати на екран.</li>
					<li className={style.task_list_item}>Робити за допомогою React.js | Typescript | Vite | Zustand | Zod.</li>
					<li className={style.task_list_item}>PS: Можна використовувати ChatGPT або якісь готові рішення.</li>
				</ul>
			</section>
			<section className={style.task_section}>
				<h2 className={style.section_title}>Виконання:</h2>
				<ul className={style.task_list}>
					<li className={style.task_list_item}>
						<h3>1. Для кращого відображення на сторінці 1(inch) = 10px.</h3>
					</li>
					<li className={style.task_list_item}>
						<h3>2. Реалізація базового алгоритму упаковки прямокутників.</h3>
						<p>
							Він намагається вмістити набір блоків у більший контейнер заданої ширини та висоти. Алгоритм використовує
							структуру даних бінарного дерева для представлення доступного простору в контейнері.
						</p>
						<Link className={style.link} to={'basic-algorithm'}>
							Переглянути результат базового алгоритму
						</Link>
					</li>
					<li className={style.task_list_item}>
						<h3>3. Реалізація складного алгоритму упаковки прямокутників.</h3>
						<p>
							Це алгоритм упаковки прямокутників на основі бінарного дерева, який є більш складним, ніж базовий
							алгоритм. Замість того, щоб починати з фіксованої ширини та висота, починається з ширини та висоти першого
							пройденого блоку, а потім зростає в міру необхідності для розміщення кожного наступного блоку. У міру
							зростання він намагається підтримувати приблизно квадратичне співвідношення, роблячи «розумний» вибір щодо
							того, чи потрібно рости праворуч або вниз.
						</p>
						<p>
							Для кращого відображення результату на мобільних пристроях реалізоване маштабування аркушу з результатом
							та фігур до розміру екрану.
						</p>
						<Link className={style.link} to={'advanced-algorithm'}>
							Переглянути результат складного алгоритму
						</Link>
					</li>
					<li className={style.task_list_item}>
						<h3>4. Додатково.</h3>
						<p>
							Реалізована функція яка змінює закінчення слів залежно від числа. (наприклад: 1 Дюйм, 2 Дюйми, 2,5 Дюйма,
							5 Блоків і тд.).
						</p>
						<p>
							Реалізована можливість додавання, редагування та видалення фігур. <br />
							Для управління станом задіяно - Zustand.
							<br />
							Для валідації даних форми задіяно - Zod.
							<br /> Форма з компонентів - Bootstrap.
						</p>
					</li>
				</ul>
			</section>
			{modalShow && (
				<ModalForm
					show={modalShow}
					onHide={() => {
						setModalShow(false);
						setBlockId(null);
					}}
					id={BlockId}
				/>
			)}
		</main>
	);
}
