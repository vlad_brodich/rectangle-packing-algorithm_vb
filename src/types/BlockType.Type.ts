export interface BlockType {
	w: number;
	h: number;
	x?: number;
	y?: number;
	num: number;
	color: string;
	fit?: FitType;
}

export interface FitType {
	x: number;
	y: number;
	w: number;
	h: number;
	used: boolean;
	top?: any;
	left?: any;
}
