import { InputBlock } from "./InputBlock.Type";

export interface FiguresListProps {
	data: InputBlock[];
	callbackModalShow: (id: number | string) => void;
}