import { SheetType } from "./SheetType.Type";

export interface ListResultProps {
	sheets: SheetType[];
	indexSize:number
}