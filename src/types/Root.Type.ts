export interface Root {
	x: number;
	y: number;
	w: number;
	h: number;
	used?: boolean;
	top?: Root;
	left?: Root;
}
