import { z } from 'zod';

export const formSchema = z.object({
	width: z
		.number().refine(w => w >= 2 && w <= 20, {
			message: 'Ширина блока може бути від 2 до 20 дюймів.'
		}),
	height: z.number().refine(h => h >= 2 && h <= 40,{
		message: 'Висота блока може бути від 2 до 20'
	}),
	count: z.number().refine(c => c > 0, {
		message: 'Кількість блоків повинна бути більше 0'
	})
});

export type FormSchema = z.infer<typeof formSchema>;
