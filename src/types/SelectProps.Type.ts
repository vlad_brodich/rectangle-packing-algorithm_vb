export interface SelectProps {
	selected: (value: string) => void;
}