import { OutputBlockType } from './OutputBlock.Type';
interface BoxSizeType {
	w: number;
	h: number;
}
export interface SheetType {
	blocks: OutputBlockType[];
	numSheet: number;
	empty: number;
	boxSize: BoxSizeType;
}
