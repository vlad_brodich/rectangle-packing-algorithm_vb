export interface BlockPropTypes {
	width: number;
	height: number;
	numBlock: number;
	x: number;
	y: number;
	color: string;
	indexSize:number
}
