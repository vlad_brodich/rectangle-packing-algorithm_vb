import { BlockType } from "./BlockType.Type";

export interface OutputBlockType extends BlockType {
	x: number;
	y: number;
}
