export interface BoxSizeType {
	w: number;
	h: number;
}