export interface ModalFormProps {
	id: number | string | null;
	onHide: () => void;
	show: boolean;
}
