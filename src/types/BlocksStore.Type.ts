import { InputBlock } from "./InputBlock.Type";

export interface BlocksStoreType {
	blocks: InputBlock[];
	addBlock: (block: InputBlock) => void;
	removeBlock: (id: number | string) => void;
	editBlock: (id: number | string, block: InputBlock) => void;
}