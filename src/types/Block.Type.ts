import { Root } from "./Root.Type";

export interface Block {
	w: number;
	h: number;
	fit?: Root | null;
}