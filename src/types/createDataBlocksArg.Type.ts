export interface createDataBlocksArgType {
	width: number;
	height: number;
	count: number;
}