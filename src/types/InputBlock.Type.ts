export interface InputBlock {
	id: number|string;
	width: number;
	height: number;
	count: number;
}