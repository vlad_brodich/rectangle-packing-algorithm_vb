export const wordPluralEndingsUk = (num: number, word: string): string => {
  if (Number.isInteger(num)) {
    if (num >=2 &&num <= 4) {
			return `${word}и`;
		} else if (num === 0 || num > 4) {
			return `${word}ів`;
		} else {
			return word;
		}
  }else return `${word}а`;
};
