 // Функція створення ID або Key.

export function createId(num: number | undefined = 6): string {
	const letters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	let newId = '';

	for (let i = 0; i < num; i++) {
		const randomIndex = Math.floor(Math.random() * letters.length);
		newId += letters.charAt(randomIndex);
	}

	return newId;
}
