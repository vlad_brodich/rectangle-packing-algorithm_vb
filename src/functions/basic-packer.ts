import { Block } from '../types/Block.Type';
import { BoxSizeType } from '../types/BoxSize.Type';
import { Root } from '../types/Root.Type';

export default class BasicPacker {
	private root: Root;

	constructor(width: number, height: number) {
		this.root = { x: 0, y: 0, w: width, h: height };
	}

	fit(blocks: Block[]): void {
		let node: Root | null = null;

		for (const block of blocks) {
			node = this.findNode(this.root, block.w, block.h);

			if (node) {
				block.fit = this.splitNode(node, block.w, block.h);
			}
		}
	}

	getBoxSize(): BoxSizeType {
		return { w: this.root.w, h: this.root.h };
	}

	private findNode(root: any, width: number, height: number): Root | null {
		if (root.used) {
			return this.findNode(root.left, width, height) || this.findNode(root.top, width, height);
		} else if (width <= root.w && height <= root.h) {
			return root;
		} else {
			return null;
		}
	}

	private splitNode(node: Root, width: number, height: number): Root {
		node.used = true;
		node.top = { x: node.x, y: node.y + height, w: node.w, h: node.h - height };
		node.left = { x: node.x + width, y: node.y, w: node.w - width, h: height };
		return node;
	}
}

// Клас BasicPacker ініціалізується шириною та висотою контейнера.
// Контейнер представлений кореневим об’єктом із властивостями x, y, w(ширина) і h(висота).

// Метод fit використовується для розміщення масиву блоків у контейнері.
// Він повторює кожен блок і намагається знайти відповідне місце для нього в контейнері.

// Метод privatefindNode — це рекурсивна функція, яка шукає вільний простір у структурі бінарного дерева контейнера.
// Він повертає перший доступний вузол, який може вмістити блок заданої ширини та висоти.

// Метод private splitNode відповідає за позначення вузла як використаного та поділ його на два нових вузла, щоб представити простір, зайнятий підігнаним блоком.

// !! Важливо зазначити, що метод private findNode повертає перший доступний вузол, який він зустрічає, що не завжди може призвести до найоптимальнішого пакування.
// Існують більш просунуті алгоритми пакування, спрямовані на підвищення ефективності та оптимізації, але ця базова реалізація служить відправною точкою.
