import { BlockType } from '../types/BlockType.Type';

// Сортування від максимальної висоти.
const sortMaxH = (a: BlockType, b: BlockType): number => {
	if (b.h == a.h) {
		return b.w * b.h - a.w * a.h;
	}
	return b.h - a.h;
};
// Сортування від мінімальної висоти.
const sortMinH = (a: BlockType, b: BlockType): number => {
	if (b.h == a.h) {
		return a.w * a.h - b.w * b.h;
	}
	return a.h - b.h;
};
// Сортування від максимальної ширини.
const sortMaxW = (a: BlockType, b: BlockType): number => {
	if (b.w == a.w) {
		return b.w * b.h - a.w * a.h;
	}
	return b.w - a.w;
};
// Сортування від мінімальної ширини.
const sortMinW = (a: BlockType, b: BlockType): number => {
	if (b.w == a.w) {
		return a.w * a.h - b.w * b.h;
	}
	return a.w - b.w;
};
// Сортування від максимальної площі фігури.
const sortMaxS = (a: BlockType, b: BlockType): number => {
	return b.w * b.h - a.w * a.h;
};
// Сортування від мінімальної площі фігури.
const sortMinS = (a: BlockType, b: BlockType): number => {
	return a.w * a.h - b.w * b.h;
};
// Тасування Фишера — Йетса.
const shuffle = (arr: BlockType[]): BlockType[] => {
	let j, temp;
	for (let i = arr.length - 1; i > 0; i--) {
		j = Math.floor(Math.random() * (i + 1));
		temp = arr[j];
		arr[j] = arr[i];
		arr[i] = temp;
	}
	return arr;
};

export function sortBlocks(data: BlockType[], key: string): BlockType[] {
	switch (key) {
		case 'maxH':
			return [...data].sort(sortMaxH);
		case 'minH':
			return [...data].sort(sortMinH);
		case 'maxW':
			return [...data].sort(sortMaxW);
		case 'minW':
			return [...data].sort(sortMinW);
		case 'maxS':
			return [...data].sort(sortMaxS);
		case 'minS':
			return [...data].sort(sortMinS);
		case 'random':
			return shuffle([...data]);
		default:
			return data;
	}
}
