import { Block } from '../types/Block.Type';
import { BoxSizeType } from '../types/BoxSize.Type';
import { Root } from '../types/Root.Type';

export class AdvancedPacker {
	private root: Root;
	constructor() {
		this.root = { x: 0, y: 0, w: 0, h: 0 };
	}

	fit(blocks: Block[]): void {
		let node: Root | null = null,
			block: Block,
			len = blocks.length;
		let w = len > 0 ? blocks[0].w : 0;
		let h = len > 0 ? blocks[0].h : 0;
		this.root = { x: 0, y: 0, w: w, h: h };
		for (let n = 0; n < len; n++) {
			block = blocks[n];
			if ((node = this.findNode(this.root, block.w, block.h))) {
				block.fit = this.splitNode(node, block.w, block.h);
			} else {
				block.fit = this.growNode(block.w, block.h);
			}
		}
	}

	getBoxSize(): BoxSizeType {
		return { w: this.root.w, h: this.root.h };
	}

	private findNode(root: any, w: number, h: number): Root | null {
		if (root.used) {
			return this.findNode(root.left, w, h) || this.findNode(root.top, w, h);
		} else if (w <= root.w && h <= root.h) {
			return root;
		} else {
			return null;
		}
	}

	private splitNode(node: Root, w: number, h: number): Root {
		node.used = true;
		node.top = { x: node.x, y: node.y + h, w: node.w, h: node.h - h };
		node.left = { x: node.x + w, y: node.y, w: node.w - w, h: h };
		return node;
	}

	private growNode(w: number, h: number): Root | null {
		let canGrowDown = w <= this.root.w;
		let canGrowRight = h <= this.root.h;

		let shouldGrowRight = canGrowRight && this.root.h >= this.root.w + w;
		let shouldGrowDown = canGrowDown && this.root.w >= this.root.h + h;

		if (shouldGrowRight) {
			return this.growRight(w, h);
		} else if (shouldGrowDown) {
			return this.growDown(w, h);
		} else if (canGrowRight) {
			return this.growRight(w, h);
		} else if (canGrowDown) {
			return this.growDown(w, h);
		} else {
			return null;
		}
	}

	private growRight(w: number, h: number): Root | null {
		this.root = {
			used: true,
			x: 0,
			y: 0,
			w: this.root.w + w,
			h: this.root.h,
			top: this.root,
			left: { x: this.root.w, y: 0, w: w, h: this.root.h }
		};
		let node;
		if ((node = this.findNode(this.root, w, h))) {
			return this.splitNode(node, w, h);
		} else {
			return null;
		}
	}

	private growDown(w: number, h: number): Root | null {
		this.root = {
			used: true,
			x: 0,
			y: 0,
			w: this.root.w,
			h: this.root.h + h,
			top: { x: 0, y: this.root.h, w: this.root.w, h: h },
			left: this.root
		};
		let node;
		if ((node = this.findNode(this.root, w, h))) {
			return this.splitNode(node, w, h);
		} else {
			return null;
		}
	}
}

/*****************************************************************************

Це алгоритм упаковки прямокутників на основі бінарного дерева, який є більш складним, ніж простий BasicPacker. 
Замість того, щоб починати з фіксованої ширини та висота, починається з ширини та висоти першого пройденого блоку, а потім зростає в міру необхідності для розміщення кожного наступного блоку.
У міру зростання він намагається підтримувати приблизно квадратичне співвідношення, роблячи «розумний» вибір щодо того, чи потрібно рости праворуч або вниз.

При зростанні алгоритм може рости лише праворуч або вниз. 
Тому, якщо новий блок ширший, і вищий за поточну ціль, він буде
відхилений. Це робить дуже важливим ініціалізацію з розумним запуском
ширина і висота. 
Якщо ви надаєте відсортований вхід (спочатку найбільший), тоді це
не буде проблемою.

Потенційним способом вирішення цього обмеження було б дозволити зростання в обох напрямках відразу, але це вимагає підтримки більш складного дерева
з 3 дітьми (внизу, праворуч і в центрі), і цієї складності можна уникнути
просто вибравши розумний стартовий блок.

Найкращі результати досягаються, коли вхідні блоки сортуються за мінімальною висотою, або навіть краще при сортуванні за максимальною площею.
*/
