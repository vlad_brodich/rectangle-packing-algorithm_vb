import { COLORS } from '../constants/constants';
import { BlockType } from '../types/BlockType.Type';
import { createDataBlocksArgType } from '../types/createDataBlocksArg.Type';

export default function createDataBlocks(data: createDataBlocksArgType[]): BlockType[] {
	const arr: BlockType[] = [];
	data.forEach((el, c) => {
		for (let i = 0; i < el.count; i++) {
			arr.push({
				w: el.width * 10,
				h: el.height * 10,
				num: i + 1,
				color: (COLORS[c] ??= 'rgb(100, 168, 10)')
			});
		}
	});
	return arr;
}
