export 	const sizeAdapter = (size: number, index: number): number => {
	return index > 0 ? size * index : size;
};
